package first;

 class Animal{
	 public void sound() {
		 System.out.println("Animals living in Forest");
	 }
 }
	 class Dog extends Animal{
	      @Override
		 public void sound() {
			 System.out.println("Dog is barking");
		 }
	 }
	 
public class MethodOverriding {
	public static void main(String[] args) {
		
		Animal dog = new Dog();
		
	     dog.sound();
	}
	
}
	


