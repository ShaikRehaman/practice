package first;


   class Add{
	   public int sum(int a, int b) {
		   return a+b;
		   
	   }
	   public int sum(int a, int b, int c ) {
			   return a+b+c;
	   }
	   public String sum(String a, String b, int c) {
			   return a+b+c;
		   }
   }
public class MethodOverloading {
	public static void main(String[] args) {
		
		Add add = new Add();
		
		System.out.println(add.sum(10, 20));
		System.out.println(add.sum(10, 20, 30));
		System.out.println(add.sum("Shaik",  "Rehman", 189));
	}

}
